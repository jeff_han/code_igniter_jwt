<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Outlets extends Migration
{
    public function up()
    {
        // Disable foreign key check temporary to prevent error
        $this->db->disableForeignKeyChecks();
        $this->forge->addField([
            'id' => [
                'type' => 'BIGINT',
                'constraint' => '20',
                'null' => 'false',
                'unsigned' => true,
                'auto_increment' => true,
            ],
            'merchant_id' => [
                'type' => 'BIGINT',
                'constaint' => '20',
                'unsigned' => true,
                'null' => false,
            ],
            'outlet_name' => [
                'type' => 'VARCHAR',
                'constraint' => '40',
                'null' => false,
            ],
            'created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
            'created_by' => [
                'type' => 'BIGINT',
                'constraint' => '20',
                'unsigned' => true,
                'null' => false,
            ],
            'updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
            'updated_by' => [
                'type' => 'BIGINT',
                'constraint' => '20',
                'unsigned' => true,
                'null' => false,
            ],
        ]);
        $this->forge->addKey('id', true);
        $this->forge->addForeignKey('merchant_id', 'merchants', 'id', 'CASCADE', 'CASCADE');
        $this->forge->createTable('outlets');
        // Enable foreign key check
        $this->db->enableForeignKeyChecks();
    }

    public function down()
    {
        $this->forge->dropTable('outlets', true);
    }
}
