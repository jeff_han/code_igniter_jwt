<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Users extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'id'	=> [
				'type'	=> 'BIGINT',
				'constraint' => '20',
				'null' => false,
				'unsigned' => true,
				'auto_increment' => true,
			],
			'name'	=> [
				'type' => 'VARCHAR',
				'constraint' => '45',
				'null' => true,
			],
			'user_name' => [
				'type' => 'VARCHAR',
				'constraint' => '45',
				'null' => true,
			],
			'password' => [
				'type' => 'VARCHAR',
				'constraint' => '225',
				'null' => true,
			],
			'created_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP',
			'created_by' => [
				'type' => 'BIGINT',
                'unsigned' => true,
				'constraint' => '20',
				'null' => false,
			],
			'updated_at TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP',
			'updated_by' => [
				'type' => 'BIGINT',
				'constraint' => '20',
                'unsigned' => true,
				'null' => false,
			],
		]);
		$this->forge->addKey('id', true);
		$this->forge->createTable('users');
	}

	public function down()
	{
		$this->forge->dropTable('users', true);	
	}
}
