<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use Firebase\JWT\JWT;

class UserController extends ResourceController
{
    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    use ResponseTrait;
    public function index($year=null, $month=null, $page=null)
    {
        $db = \Config\Database::connect();
        $key = getenv('TOKEN_SECRET');
        $header = $this->request->getServer('HTTP_AUTHORIZATION');
        if(!$header) return $this->failUnauthorized('Token Required');
        $token = explode(' ', $header)[1];


        try {
            $decoded =JWT::decode($token, $key, ['HS256']);
            $user = [
                'id' => $decoded->uid,
                'name' => $decoded->name,
            ];

        } catch (\Throwable $e) {
            return $this->fail('Invalid Token');
   	 	}

        if ($page == null || $page > 3 || $page < 1) {
            $page=1;
        }
        $user["page"] = $page;
        if ($page > 1 && $page <= 3){
            $page = ($page-1)*10+1;
        }
        if ($year == null || $year > 2021 || $year < 2000) {$year=2021;}
        if ($month == null || $month > 12 || $month < 1) {$month=11;}
        $end = $page+10;
        
        $qr_c = $db->query("SELECT merchants.id, merchant_id, user_id, merchant_name, SUM(bill_total) as omzet, DATE_FORMAT(transactions.created_at, '%Y-%m-%d') as date, transactions.created_by FROM transactions JOIN merchants ON merchants.id = transactions.merchant_id WHERE user_id = $decoded->uid and transactions.created_at >= '$year:$month:$page' and transactions.created_at <= '$year:$month:$end' GROUP BY date");
        $result_c = $qr_c->getResultArray();

        $qr_d = $db->query("SELECT date, merchant_name, outlet_name, omzet FROM (SELECT merchants.id, outlet_id, merchant_id, user_id, merchant_name, SUM(bill_total) as omzet, DATE_FORMAT(transactions.created_at, '%Y-%m-%d') as date, transactions.created_by FROM transactions JOIN merchants ON merchants.id = transactions.merchant_id WHERE user_id = $decoded->uid  and transactions.created_at >= '$year:$month:$page' and transactions.created_at <= '$year:$month:$end' GROUP BY date) as mt JOIN outlets ON mt.outlet_id = outlets.id ORDER BY date");
        $result_d = $qr_d->getResultArray();

        $report_c = [];
        $line = 0;
        for ($i=$page; $i<$end; $i++){
            $date = sprintf('%02d', $i);
            if (!empty($result_c[$line]) && $result_c[$line]["date"] == "2021-11-$date"){
                $report_c["2021-11-$date"] =  ["merchant_name"=>$result_c[$line]["merchant_name"], "omzet"=>$result_c[$line]["omzet"]];
                $line++;
            }else{
                 $report_c["2021-11-$date"] = ["merchant_name"=>"", "omzet"=>0];
            }
        }

        $report_d = [];
        $line = 0;
        for ($i=$page; $i<$end; $i++){
            $date = sprintf('%02d', $i);
            if (!empty($result_d[$line]) && $result_d[$line]["date"] == "2021-11-$date"){
                $report_d["2021-11-$date"] =  ["merchant_name"=>$result_d[$line]["merchant_name"], "outlet_name"=>$result_d[$line]["outlet_name"], "omzet"=>$result_d[$line]["omzet"]];
                $line++;
            }else{
                 $report_d["2021-11-$date"] = ["merchant_name"=>"", "outlet_name"=>"", "omzet"=>0];
            }
        }


        $response = [
            "user" => $user,
            "report_c" => $report_c,
            "report_d" => $report_d,
        ];

        return $this->setResponseFormat('json')->respond($response);
	}


    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        //
    }

    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    public function create()
    {
        //
    }

    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        //
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        //
    }
}
