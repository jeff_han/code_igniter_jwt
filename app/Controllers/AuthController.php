<?php

namespace App\Controllers;

use CodeIgniter\RESTful\ResourceController;
use CodeIgniter\API\ResponseTrait;
use App\Models\UserModel;
use Firebase\JWT\JWT;

class AuthController extends ResourceController
{
    /**
     * Create a new resource object, from "posted" parameters
     *
     * @return mixed
     */
    use ResponseTrait;
    public function register()
    {
        helper(['form']);
        $rules = [
            'name' => 'required',
            'user_name' => 'required',
            'password' => 'required|min_length[8]'
        ];
        if(!$this->validate($rules)) return $this->fail($this->validator->getErrors());
        $data = [
            'name' => $this->request->getVar('name'),
            'user_name' => $this->request->getVar('user_name'),
            'password' => password_hash($this->request->getVar('password')),
        ];
        $model = new UserModel();
        $user = $model->save($data);
        $this->respondCreated($user);
    }

    /**
     * Return an array of resource objects, themselves in array format
     *
     * @return mixed
     */
    use ResponseTrait;
    public function login()
    {
        helper(['form']);
        $rules = [
            'user_name' => 'required',
            'password' => 'required'
        ];
        if(!$this->validate($rules)) return $this->fail($this->validator->getErrors());
        $model = new UserModel();
        $user = $model->where("user_name", $this->request->getVar('user_name'))->first();
        if(!$user) return $this->failNotFound('You are not registered');
        
        // Verify the password
        $verify = password_verify($this->request->getVar('password'), $user['password']);
        if(!$verify) return $this->fail('Login failed, please check your username and password');
        // Create payload and token
        $key = getenv('TOKEN_SECRET');
        $iat = time();
        $nbf = $iat + 10;
        $exp = $iat + 3600;
        $payload = array(
            "iss" => "merchant-id.com",
            "uid" => $user['id'],
            "name" => $user['name'],
            "iat" => $iat,
            "nbf" => $nbf,
            "exp" => $exp
        );
        $token = JWT::encode($payload, $key);

        return $this->setResponseFormat('json')->respond(["token" => $token]);
    }

    /**
     * Return the properties of a resource object
     *
     * @return mixed
     */
    public function logout($id = null)
    {
        //
    }

    /**
     * Return a new resource object, with default properties
     *
     * @return mixed
     */
    public function new()
    {
        //
    }
    /**
     * Return the editable properties of a resource object
     *
     * @return mixed
     */
    public function edit($id = null)
    {
        //
    }

    /**
     * Add or update a model resource, from "posted" properties
     *
     * @return mixed
     */
    public function update($id = null)
    {
        //
    }

    /**
     * Delete the designated resource object from the model
     *
     * @return mixed
     */
    public function delete($id = null)
    {
        //
    }
}
